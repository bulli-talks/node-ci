var express = require('express')
var app = express()

app.get('/users', (req, res, next) => {
  res.json([
    { age: 26, name: 'Pedro Perez', company: 'ESCHOIR', email: 'beckshelton@eschoir.com' },
    { age: 36, name: 'Juan Lopez', company: 'EARTHPURE', email: 'onealkirby@earthpure.com' },
    { age: 35, name: 'Byrd Mcgowan', company: 'TERRASYS', email: 'byrdmcgowan@terrasys.com' },
    { age: 21, name: 'Aguirre Weiss', company: 'COMSTAR', email: 'aguirreweiss@comstar.com' },
    { age: 27, name: 'Duffy Ford', company: 'BLURRYBUS', email: 'duffyford@blurrybus.com' }
  ])
})

app.use(express.static('public'))

var port = process.env.PORT || 3000
app.listen(port, () => {
  console.log(`Server running on port ${port}`)
})
